#!/bin/bash

flask db upgrade
python seed.py

gunicorn -b 0.0.0.0:8000 app:app

